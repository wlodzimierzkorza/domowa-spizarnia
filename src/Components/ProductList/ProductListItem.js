import React, { useState } from 'react';
import styled from 'styled-components';
import {
  Col,
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap';

const StyledCard = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  padding: 20px;
  background-color: ${({ theme }) => theme.bg};
  border: solid 1px ${({ theme }) => theme.gray100};
  border-radius: 5px;
  margin: 15px auto;
  h3 {
    width: 80%;
    font-weight: 500;
  }
  span {
    padding: 0 10px;
    color: ${({ theme }) => theme.gray200};
    font-weight: 100;
    width: 100px;
  }
`;
const StyledContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  margin: 5px 0;
`;

const StyledProgress = styled.div`
  width: 80%;
  margin-right: 10px;
  height: 2px;
  background-color: ${({ theme }) => theme.gray100};
  position: relative;

  &::before {
    content: '';
    width: ${({ value }) => value}%;
    position: absolute;
    left: -2px;
    background-color: ${({ limit, now, theme }) =>
      parseInt(limit, 2) > parseInt(now, 2) ? theme.red : theme.green};
    height: 2px;
    color: ${({ theme }) => theme.gray200};
  }

  &::after {
    font-weight: 100;
    color: ${({ theme }) => theme.gray200};
    content: '';
    position: absolute;
    right: -5px;
  }

  span {
    display: block;
    width: 30px;
    height: 30px;
    background-color: ${({ limit, now, theme }) =>
      parseInt(limit, 2) > parseInt(now, 2) ? theme.red : theme.green};
    border-radius: 50%;
    padding: 4px 0 0 0;
    position: absolute;
    text-align: center;
    top: -15px;
    left: ${({ value }) => value}%;
    color: #fff;
    font-weight: 500;
  }
  &.danger {
    ::before {
      background-color: ${({ theme }) => theme.red};
    }
    span {
      background-color: ${({ theme }) => theme.red};
    }
  }
  &.normal {
    ::before {
      background-color: ${({ theme }) => theme.green};
    }
    span {
      background-color: ${({ theme }) => theme.green};
    }
  }
`;

const ProductListItem = ({ item, redirectHandler, modalHandler }) => {
  const { name, max, unit, limit, now, id } = item;
  const percent = (now * 100) / max;
  const [isOpen, toggleOpen] = useState(false);

  const toggleHandler = () => {
    toggleOpen(!isOpen);
  };

  return (
    <Col md={6}>
      <StyledCard>
        <StyledContainer>
          <h3>{name}</h3>
          <span>{`limit ${limit}`}</span>
          <Dropdown isOpen={isOpen} toggle={toggleHandler}>
            <DropdownToggle caret nav color="info" size="lg">
              Opcje
              <DropdownMenu>
                <DropdownItem onClick={() => redirectHandler('product', id)}>
                  Edutuj
                </DropdownItem>
                <DropdownItem onClick={() => modalHandler(id)}>
                  Usuń
                </DropdownItem>
              </DropdownMenu>
            </DropdownToggle>
          </Dropdown>
        </StyledContainer>
        <StyledContainer>
          <span>0</span>
          <StyledProgress
            value={percent || 0}
            limit={limit}
            now={now}
            // eslint-disable-next-line radix
            className={parseInt(limit) >= parseInt(now) ? 'danger' : 'normal'}
          >
            <span>{now}</span>
          </StyledProgress>

          <span>{`${max} ${unit}`}</span>
        </StyledContainer>
      </StyledCard>
    </Col>
  );
};
export default ProductListItem;
