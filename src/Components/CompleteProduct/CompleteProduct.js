import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Button } from 'reactstrap';
import AddProductContainer from 'Components/AddProductContainer/AddProductContainer';

const StyledRange = styled.div`
  input[type='range'] {
    width: 300px;
    height: 30px;
    margin: 20px 0;
    background: transparent;
  }

  input[type='range'] {
    -webkit-appearance: none;
    width: 100%;
  }

  input[type='range']::-webkit-slider-thumb {
    -webkit-appearance: none;
  }

  input[type='range']:focus {
    outline: none;
  }

  input[type='range']::-ms-track {
    width: 100%;
    cursor: pointer;
  }

  input[type='range']::-webkit-slider-thumb {
    -webkit-appearance: none;
    height: 36px;
    width: 36px;
    background-color: ${({ theme }) => theme.green};
    border-radius: 50%;
    cursor: pointer;
    margin-top: -12px;
  }

  input[type='range']::-moz-range-thumb {
    height: 36px;
    width: 36px;
    background-color: ${({ theme }) => theme.green};
    cursor: pointer;
  }

  input[type='range']::-ms-thumb {
    height: 36px;
    width: 36px;
    background-color: ${({ theme }) => theme.green};
    cursor: pointer;
  }

  input[type='range']::-webkit-slider-runnable-track {
    width: 100%;
    height: 15px;
    cursor: pointer;
    background-color: ${({ theme }) => theme.gray100};
    border-radius: 5px;
  }

  input[type='range']:active::-webkit-slider-runnable-track {
    background-color: ${({ theme }) => theme.gray100};
  }

  input[type='range']::-moz-range-track {
    width: 100%;
    height: 12px;
    cursor: pointer;
    background-color: ${({ theme }) => theme.gray100};
    border-radius: 5px;
  }

  input[type='range']::-ms-track {
    width: 100%;
    height: 12px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
    border-radius: 5px;
  }

  input[type='range']::-ms-fill-lower {
    background-color: ${({ theme }) => theme.gray100};
  }

  input[type='range']:focus::-ms-fill-lower {
    background-color: ${({ theme }) => theme.gray100};
  }

  input[type='range']::-ms-fill-upper {
    background-color: ${({ theme }) => theme.gray100};
  }

  input[type='range']:focus::-ms-fill-upper {
    background-color: ${({ theme }) => theme.gray100};
  }
`;

const StyledInput = styled.input`
  width: 60px;
  height: 60px;
  border-radius: 10px;
  width: 100%;
  text-align: center;
  font-size: 4rem;
  border: none;
  font-weight: 700;
`;

const CompleteProduct = ({ toggle, toggleChange, product, changeProduct }) => {
  const [prod, setProduct] = useState({
    name: '',
    max: 0,
    limit: 0,
    now: 0,
    unit: '',
  });
  useEffect(() => {
    setProduct(product);
  }, [product]);

  const [range, changeRange] = useState(0);

  const toggleChangeHandler = () => {
    toggleChange(!toggle);
  };

  const changeRangeHandler = (e) => {
    changeRange(e.target.value);
  };

  const saveChangeHandler = () => {
    prod.now = parseFloat(prod.now, 1) + parseFloat(range, 1);
    toggleChangeHandler();
    changeProduct(prod);
  };
  return (
    <AddProductContainer toggle={toggle} toggleChange={toggleChange}>
      <h1>Uzupełnij produkt</h1>
      <h3>{prod.name}</h3>
      <StyledInput value={range} />
      <StyledRange>
        <input
          type="range"
          min="0"
          max={prod.max - prod.now || 0}
          step="1"
          value={range}
          onChange={changeRangeHandler}
          readOnly={false}
        />
      </StyledRange>
      <p>{`Obecnie posiadasz w spiżarni ${prod.now} ${
        prod.unit
      }. Po uzupełnieniu będziesz mieć ${
        parseFloat(prod.now, 1) + parseFloat(range, 1)
      } ${prod.unit}
      `}</p>
      <Button color="success" outline onClick={saveChangeHandler}>
        Uzupełnij
      </Button>
    </AddProductContainer>
  );
};
export default CompleteProduct;
