import React, { useState } from 'react';
import styled from 'styled-components';
import { Input, Button, FormGroup, InputGroup, Alert } from 'reactstrap';
import AddProductContainer from 'Components/AddProductContainer/AddProductContainer';
import axios from 'ax';

const AlertContainer = styled.div`
  padding-top: 30px;
`;

const AddProduct = ({ toggle, toggleChange, addProduct, units }) => {
  const [product, setProduct] = useState({
    id: '',
    name: '',
    limit: '',
    now: '',
  });
  const [alert, toggleAlert] = useState(false);

  const addProductHandler = () => {
    // eslint-disable-next-line no-unused-expressions
    product.now > product.max ? (product.now = product.max) : product.now;
    axios.post('/products.json', product).then((res) => {
      product.id = res.data.name;
      toggleAlert(true);
      setTimeout(() => {
        toggleChange(!toggle);
        addProduct(product);
        setProduct({
          id: '',
          name: '',
          limit: '',
          now: '',
        });
        toggleAlert(false);
      }, 2000);
    });
  };
  const onChangeHandler = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value });
  };

  const { name, max, limit, now } = product;

  return (
    <AddProductContainer toggle={toggle} toggleChange={toggleChange}>
      <h1>Dodaj produkt</h1>
      <FormGroup>
        <Input
          type="text"
          placeholder="Nazwa produktu"
          name="name"
          onChange={onChangeHandler}
          value={name}
          required
        />
      </FormGroup>
      <FormGroup>
        <Input type="select" name="unit" onChange={onChangeHandler}>
          <option>Wybierz jednostkę</option>
          {units &&
            units.map((item) => {
              return (
                <option value={item.value} key={item.name}>
                  {item.name}
                </option>
              );
            })}
        </Input>
      </FormGroup>
      <FormGroup>
        <InputGroup>
          <Input
            type="number"
            placeholder="Obecny stan"
            name="now"
            value={now}
            onChange={onChangeHandler}
            required
          />
        </InputGroup>
      </FormGroup>
      <FormGroup>
        <Input
          type="number"
          placeholder="Średnie zuyżcie miesięczne"
          name="max"
          value={max}
          onChange={onChangeHandler}
        />
      </FormGroup>
      <FormGroup>
        <Input
          type="number"
          placeholder="minimalna ilość"
          name="limit"
          value={limit}
          onChange={onChangeHandler}
        />
      </FormGroup>

      <Button color="success" outline onClick={addProductHandler}>
        Dodaj
      </Button>
      <AlertContainer>
        <Alert
          color="success"
          isOpen={alert}
        >{`Produkt ${product.name} został dodany`}</Alert>
      </AlertContainer>
    </AddProductContainer>
  );
};
export default AddProduct;
