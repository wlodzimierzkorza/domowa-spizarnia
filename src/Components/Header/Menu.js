import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import logo from 'addons/pantry.png';
import checklist from 'addons/checklist.png';
import settings from 'addons/adjust.png';

const StyledMenu = styled.nav`
  font-size: 1.6rem;
  a {
    margin: 0 10px;
  }
  @media (max-width: 576px) {
    position: fixed;
    display: flex;
    align-content: space-between;
    justify-content: center;
    bottom: 0;
    background-color: ${({ theme }) => theme.orange};
    height: 90px;
    z-index: 100;
    width: 100vw;
    padding: 25px 0px 25px 0px;
    left: 0;
    a {
      text-align: center;

      margin: 0;
      padding: 10px 40px;
    }
  }
`;
const StyledImage = styled.img`
  @media (max-width: 576px) {
    display: block;
    width: 40px;
  }
  display: none;
`;

const StyledText = styled.span`
  @media (max-width: 576px) {
    display: none;
  }
`;
const Menu = () => (
  <StyledMenu>
    <Link to="/">
      <StyledText>Moja spiżarnia</StyledText>
      <StyledImage src={logo} />
    </Link>
    <Link to="/shopping-list">
      <StyledText>Lista zakupów</StyledText>
      <StyledImage src={checklist} />
    </Link>
    <Link to="/properties">
      <StyledText>Ustawienia</StyledText>
      <StyledImage src={settings} />
    </Link>
  </StyledMenu>
);
export default Menu;
