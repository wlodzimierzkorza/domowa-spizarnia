import React from 'react';
import styled from 'styled-components';
import Menu from 'Components/Header/Menu';
import { Link } from 'react-router-dom';
import logo from 'addons/pantry.png';

const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px;
  margin-bottom: 20px;
  border-bottom: solid 1px ${({ theme }) => theme.gray100};
  background-color: ${({ theme }) => theme.orange};
  color: ${({ theme }) => theme.headerText};
  a {
    color: ${({ theme }) => theme.headerText};
  }
  @media (max-width: 576px) {
    position: fixed;
    width: 100vw;
    z-index: 200;
  }
`;
const StyledImage = styled.img`
  width: 40px;
`;
const Header = () => (
  <StyledHeader>
    <Link to="/">
      <StyledImage src={logo} />
    </Link>

    <Menu />
  </StyledHeader>
);
export default Header;
