import React from 'react';
import styled from 'styled-components';
import { Button } from 'reactstrap';

const StyledConatiner = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: start;
  text-align: center;
  height: 100vh;
  width: 40vw;
  right: ${({ toggle }) => (toggle === true ? `0` : '-45vw')};
  top: 0;
  z-index: 400;
  position: fixed;
  transition: right 0.3s;
  background-color: ${({ theme }) => theme.bg};
  box-shadow: 0 5px 55px 5px rgba(0, 0, 0, 0.4);
  padding: 60px 30px;

  @media (max-width: 576px) {
    width: 100vw;
    height: 100vh;
    right: ${({ toggle }) => (toggle ? '0' : '-110vw')};
  }
`;
const StyledClose = styled.div`
  position: absolute;
  bottom: 10vh;
  left: 0;
  width: 50%;
  margin-left: 25%;
`;

const AddProductContainer = ({ children, toggle, toggleChange }) => {
  return (
    <StyledConatiner toggle={toggle}>
      {children}
      <StyledClose>
        <Button block color="info" onClick={toggleChange}>
          Zamknij
        </Button>
      </StyledClose>
    </StyledConatiner>
  );
};
export default AddProductContainer;
