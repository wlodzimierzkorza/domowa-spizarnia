import React from 'react';
import { ListGroupItem, Button } from 'reactstrap';
import styled from 'styled-components';

const StyledItem = styled.div`
  padding: 10px 10px 3px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ShoppingListItem = ({ item, toggleChange }) => {
  const { name } = item;
  return (
    <ListGroupItem>
      <StyledItem>
        <h3>{name}</h3>
        <span>{`limit ${item.limit}`}</span>
        <Button color="success" outline onClick={toggleChange}>
          Uzupełnij
        </Button>
      </StyledItem>
    </ListGroupItem>
  );
};
export default ShoppingListItem;
