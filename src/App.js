import React, { useState, useEffect } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import { lightTheme, darkTheme } from 'theme/theme';
import RootView from 'Views/RootView';
import PropertiesView from 'Views/PropertiesView';
import GlobalStyles from 'theme/GlobalStyles';
import Header from 'Components/Header/Header';
import AddProduct from 'Components/AddProduct/AddProduct';
import axios from 'ax';
import SingLeProductView from 'Views/SingleProductVew';
import ShoppingList from 'Views/ShoppingListView';
import { Button, ButtonGroup } from 'reactstrap';

const StyledLayout = styled.div`
  height: 100vh;
  overflow: hidden;
  @media (max-width: 576px) {
    padding-top: 90px;
    padding-bottom: 100px;
    display: block;
    width: 100vw;
    height: 90vh;
    position: relative;
    overflow: scroll;
  }
`;

const StyledAddButton = styled.button`
  border: none;
  width: 70px;
  height: 70px;
  background-color: ${({ theme }) => theme.orange};
  border-radius: 50%;
  font-size: 1.8rem;
  color: #fff;
  position: fixed;
  right: 30px;
  bottom: 30px;
  z-index: 250;

  @media (max-width: 576px) {
    left: calc(50vw - 35px);
    bottom: 65px;
    box-shadow: 0px 5px 5px 2px #ed8d10;
  }
`;

const StyledModal = styled.div`
  position: absolute;
  width: 80vw;
  height: 60vh;
  left: 10vw;
  top: 15vh;
  border-radius: 10px;
  padding: 30px;
  background-color: #fff;
  z-index: 300;
  box-shadow: 0px 10px 10px 10px rgba(0, 0, 0, 0.2);
  display: ${({ modal }) => (modal ? 'block' : 'none')};
  h1 {
    text-align: center;
    font-weight: 700;
    color: ${({ theme }) => theme.gray200};
    margin-bottom: 60px;
  }
  .btn-group {
    width: 100%;
  }
`;
const App = ({ history }) => {
  const [toggle, changeToggle] = useState(false);
  const [modal, toggleModal] = useState(false);
  const [delItem, setDelete] = useState('');
  const [themeStyle, setThemeStyle] = useState(true);
  const [units, setUnits] = useState([]);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios.get('/products.json').then((res) => {
      const productsArray = [];

      if (res.data) {
        Object.keys(res.data).forEach((key) => {
          const obj = res.data[key];
          obj.id = key;
          productsArray.push(obj);
        });

        setProducts(productsArray);
      }
    });

    const unitsArray = [];
    axios.get('units.json').then((res) => {
      if (res.data) {
        Object.keys(res.data).forEach((key) => {
          res.data[key].id = key;
          unitsArray.push(res.data[key]);
        });
      }
      setUnits(unitsArray);
    });
  }, []);

  const updateProductsHandler = (product) => {
    axios.put(`products/${product.id}.json`, product).then(() => {
      const index = products.findIndex((item) => {
        return item.id === product.id;
      });
      const newProducts = [...products];
      newProducts[index] = product;
      setProducts(newProducts);
    });
  };

  const addProductHandler = (product) => {
    const newProducts = [...products];
    newProducts.push(product);
    setProducts(newProducts);
  };

  const changeTheme = (theme) => {
    if (theme === 'dark') {
      setThemeStyle(false);
    } else {
      setThemeStyle(true);
    }
  };

  const redirectHandler = (page, id) => {
    history.push(`${page}/${id}`);
  };

  const toggleChange = () => {
    changeToggle(!toggle);
  };

  const deleteHandler = () => {
    const newProducts = products.filter((item) => {
      return item.id !== delItem;
    });
    axios.delete(`/products/${delItem}.json`).then(() => {
      setProducts(newProducts);
      toggleModal(!modal);
    });
  };

  const modalHandler = (id) => {
    setDelete(id);
    toggleModal(!modal);
  };

  return (
    <ThemeProvider theme={themeStyle ? lightTheme : darkTheme}>
      <GlobalStyles />
      <StyledModal modal={modal}>
        <h1>Czy jesteś pewien że chcesz usunąć ten produkt?</h1>
        <ButtonGroup>
          <Button color="danger" outline onClick={deleteHandler}>
            Usuń
          </Button>
          <Button color="primary" outline onClick={() => toggleModal()}>
            Anuluj
          </Button>
        </ButtonGroup>
      </StyledModal>
      <Header />
      <StyledLayout>
        <Switch>
          <Route exact path="/">
            <RootView
              products={products}
              updateProductsHandler={updateProductsHandler}
              redirectHandler={redirectHandler}
              deleteHandler={deleteHandler}
              modalHandler={modalHandler}
            />
          </Route>
          <Route path="/properties">
            <PropertiesView changeTheme={changeTheme} />
          </Route>
          <Route path="/shopping-list">
            <ShoppingList
              products={products}
              changeProduct={updateProductsHandler}
            />
          </Route>
          <Route path="/product/:id">
            <SingLeProductView
              products={products}
              units={units}
              changeProduct={updateProductsHandler}
            />
          </Route>
        </Switch>
        <StyledAddButton onClick={() => toggleChange()}>+</StyledAddButton>
      </StyledLayout>
      <AddProduct
        toggle={toggle}
        toggleChange={toggleChange}
        addProduct={addProductHandler}
        units={units}
      />
    </ThemeProvider>
  );
};

export default withRouter(App);
