import React, { useEffect, useState } from 'react';
import {
  Fade,
  Container,
  Card,
  CardBody,
  Label,
  FormGroup,
  Input,
  Button,
} from 'reactstrap';
import { withRouter } from 'react-router-dom';

const SingleProductView = ({
  match,
  products,
  units,
  changeProduct,
  history,
}) => {
  const product = products.find((item) => {
    return item.id === match.params.id;
  });
  const [prod, setProduct] = useState({
    name: '',
    value: '',
  });
  useEffect(() => {
    setProduct(product);
  }, [product]);

  const changeProductHandler = (e) => {
    const { name, value } = e.target;
    setProduct({ ...prod, [name]: value });
  };
  const saveChangeProductHandler = () => {
    // eslint-disable-next-line no-unused-expressions
    // prod.now > prod.max ? (prod.now = prod.max) : prod.now;
    changeProduct(prod);
    history.push('/');
  };

  if (prod) {
    const { name, limit, max, unit, now } = prod;
    return (
      <Fade>
        <Container>
          <Card>
            <CardBody>
              <FormGroup>
                <Label>Nazwa produktu</Label>
                <Input
                  name="name"
                  value={name}
                  onChange={changeProductHandler}
                />
              </FormGroup>
              <FormGroup>
                <Label>Jednostka</Label>
                <Input
                  name="unit"
                  type="select"
                  onChange={changeProductHandler}
                >
                  {units &&
                    units.map((item) => {
                      const selected = unit === item.value;
                      return (
                        <option
                          value={item.value}
                          selected={selected}
                          key={item.name}
                        >
                          {item.name}
                        </option>
                      );
                    })}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Minimalna ilosć</Label>
                <Input
                  name="limit"
                  value={limit}
                  onChange={changeProductHandler}
                  type="number"
                />
              </FormGroup>
              <FormGroup>
                <Label>Obecny stan</Label>
                <Input
                  type="number"
                  name="now"
                  value={now}
                  onChange={changeProductHandler}
                />
              </FormGroup>
              <FormGroup>
                <Label>Średnie miesięczne zużycie</Label>
                <Input
                  type="number"
                  name="max"
                  value={max}
                  onChange={changeProductHandler}
                />
              </FormGroup>
              <Button onClick={saveChangeProductHandler}>Zapisz</Button>
            </CardBody>
          </Card>
        </Container>
      </Fade>
    );
  }
  return (
    <Fade>
      <h3>Nie ma</h3>
    </Fade>
  );
};
export default withRouter(SingleProductView);
