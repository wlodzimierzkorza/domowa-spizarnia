import React, { useState } from 'react';
import {
  Container,
  Col,
  Row,
  Card,
  CardHeader,
  CardBody,
  Fade,
  Button,
  ButtonGroup,
  Input,
  FormGroup,
} from 'reactstrap';
import styled from 'styled-components';
import axios from 'ax';

const StyledButtons = styled.div`
  display: flex;
  .btn-group {
    width: 100%;
  }
`;
const PropertiesView = ({ changeTheme }) => {
  const [unit, setUnit] = useState({
    name: '',
    value: '',
  });
  const [push, setPush] = useState({});

  const changeUnitHandler = (e) => {
    setUnit({ ...unit, [e.target.name]: e.target.value });
  };
  const addUnitHandler = () => {
    axios.post('units.json', unit).then(() => {
      setUnit({ name: '', value: '' });
    });
  };
  const addPushHandler = () => {
    localStorage.setItem('pushNote', JSON.stringify(push));
  };

  const changePushHandler = (e) => {
    const value = parseInt(e.target.value);

    const date = new Date().getTime();
    const newPush = {
      checkDate: date + value,
      interveal: value,
    };
    setPush(newPush);
  };
  console.log(push);
  return (
    <Fade>
      <Container>
        <Row>
          <Col md={6}>
            <Card>
              <CardHeader>Ustawienia skórki</CardHeader>
              <CardBody>
                <StyledButtons>
                  <ButtonGroup>
                    <Button outline onClick={() => changeTheme('light')}>
                      Jasna
                    </Button>
                    <Button onClick={() => changeTheme('dark')}>Ciemna</Button>
                  </ButtonGroup>
                </StyledButtons>
              </CardBody>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <CardHeader>Dodaj jednostki</CardHeader>
              <CardBody>
                <FormGroup>
                  <Input
                    name="name"
                    placeholder="nazwa jednostki"
                    onChange={changeUnitHandler}
                    value={unit.name}
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    name="value"
                    placeholder="jednostka"
                    onChange={changeUnitHandler}
                    value={unit.value}
                  />
                </FormGroup>
                <Button color="success" outline block onClick={addUnitHandler}>
                  Dodaj
                </Button>
              </CardBody>
            </Card>
          </Col>
          <Col md={6}>
            <Card>
              <CardHeader>Ustaw powiadomienia</CardHeader>
              <CardBody>
                <FormGroup>
                  <Input
                    type="select"
                    onChange={changePushHandler}
                    value={unit.name}
                  >
                    <option value="0">Wybierz wartość</option>
                    <option value="60000">Co minutę</option>
                    <option value="86400000">Co 1 dzień</option>
                    <option value="604800000">Co tydzień</option>
                  </Input>
                </FormGroup>

                <Button color="success" outline block onClick={addPushHandler}>
                  Dodaj
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fade>
  );
};
export default PropertiesView;
