import React from 'react';
import { Fade, Row, Container } from 'reactstrap';
import ProductListItem from 'Components/ProductList/ProductListItem';

const RootView = ({ products, redirectHandler, modalHandler }) => {
  return (
    <Fade>
      <Container>
        <Row>
          {products &&
            products.map((item) => {
              return (
                <ProductListItem
                  item={item}
                  key={item.id}
                  redirectHandler={redirectHandler}
                  modalHandler={modalHandler}
                />
              );
            })}
        </Row>
      </Container>
    </Fade>
  );
};
export default RootView;
