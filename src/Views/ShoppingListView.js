/* eslint-disable radix */
import React, { useState } from 'react';
import { Fade, Container, ListGroup } from 'reactstrap';
import CompleteProduct from 'Components/CompleteProduct/CompleteProduct';
import ShoppingListItem from 'Components/ShoppingList/ShoppingListItem';
import styled from 'styled-components';

const StyledText = styled.h1`
  font-size: 5rem;
  font-weight: 700;
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  text-align: center;
  padding-top: 20vh;
  color: ${({ theme }) => theme.gray200};
`;

const ShoppingList = ({ products, changeProduct }) => {
  const [toggle, toggleChange] = useState(false);
  const [product, setItem] = useState({});

  const shopList = products.filter((item) => {
    return parseInt(item.now) <= parseInt(item.limit);
  });

  const toggleChangeHandler = (id) => {
    const obj = shopList.find((item) => {
      return item.id === id;
    });
    setItem(obj);
    toggleChange(!toggle);
  };

  const emptyProduct = {
    name: '',
    max: 0,
    limit: 0,
    now: 0,
    unit: '',
  };

  if (shopList.length > 0) {
    return (
      <Fade>
        <Container>
          <ListGroup>
            {shopList.map((item) => {
              return (
                <ShoppingListItem
                  item={item}
                  key={item.id}
                  toggleChange={() => toggleChangeHandler(item.id)}
                />
              );
            })}
          </ListGroup>
        </Container>
        <CompleteProduct
          toggle={toggle}
          toggleChange={toggleChangeHandler}
          product={product || emptyProduct}
          changeProduct={changeProduct}
        />
      </Fade>
    );
  }
  return (
    <Fade>
      <StyledText>Lista zakupów jest pusta</StyledText>
    </Fade>
  );
};
export default ShoppingList;
