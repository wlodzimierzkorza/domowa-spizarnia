import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
*,*::before,*::after{
    box-sizing: border-box;
    margin:0;
    padding:0;
    outline:none;
    };
    html{
      font-size:62.5%; 
    };
    body{
    font-family: 'Roboto', sans-serif;
    font-size:1.4rem;
    font-weight:400;
    background-color: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text}
}
.form-control{
    font-size:1.8rem ;
    font-weight:300;
    height:50px;
}
.btn{
    font-size:1.8rem;
    height:40px;
}
.dropdown-menu{
    font-size:1.4rem
}

}


`;

export default GlobalStyle;
