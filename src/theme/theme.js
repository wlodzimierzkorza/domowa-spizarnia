export const lightTheme = {
  body: '#FEFDFC',
  text: '#151515',
  gray100: '#e5e5e5',
  gray200: '#8c8c8c',
  bg: '#ffffff',
  light: 300,
  bold: 700,
  orange: '#FA9E20',
  headerText: '#fff',
  green: '#27A745',
  red: '#FF0000',
};

export const darkTheme = {
  body: '#303030',
  text: '#151515',
  gray100: '#e5e5e5',
  gray200: '#8c8c8c',
  bg: '#ffffff',
  light: 300,
  bold: 700,
  orange: '#FA9E20',
  headerText: '#fff',
  green: '#27A745',
  red: '#FF0000',
};
